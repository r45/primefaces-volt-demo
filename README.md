#primefaces-volt-demo
PrimeFaces的Volt布局与主题的演示程序

## 项目说明
primefaces-volt与primefaces-volt-demo是对[官网volt演示程序](http://www.primefaces.org/volt/)进行分析，通过逆向工程得到的。
换句话说，就是crack，没钱的程序员想[吃霸王餐](http://www.primefaces.org/layouts/volt)。
这两个项目现在已经可以运行了 :-)
欢迎其他人员加入，大家一起交流、测试、改进、试用，都欢迎。

QQ群“JSF,EJB,JPA,SHIRO,CDI”，群号：25882838

QQ群“primefaces”，群号：31551213

## 项目介绍
首先，其实有两个项目：

一个是[primefaces-volt](http://git.oschina.net/ratking/primefaces-volt)，它提供Volt响应式页面布局(layout)与主题风格(theme)，
最后会生成一个primefaces-volt.jar包，供基于PrimeFaces的Web项目使用。
使用的技术包括JSF 2.2、jQuery、CSS……

另一个是[primefaces-volt-demo](http://git.oschina.net/ratking/primefaces-volt-demo)，这是一个使用primefaces-volt.jar的web演示程序，
最后会生成一个primefaces-volt-demo.war包，可以部署到Java EE 7（比如GlassFish 4.x或WildFly 9.x）里运行。
使用的技术主要是PrimeFaces 5.3、JSF 2.2、CDI 1.2、一点儿HTML 5、OmniFaces 2.2……

## Volt布局与主题预览
Volt提供4种背景颜色（深蓝、深红、铅灰色、深绿）
<p align="center"><img src="http://www.primefaces.org/images/market/volt/quality.png" alt="Volt提供4种背景颜色"/></p>

Volt布局
<p align="center"><a href="http://www.primefaces.org/images/market/volt/phones.png" title="点击查看大图"><img src="http://www.primefaces.org/images/market/volt/volt-layout.png" alt="Volt布局"/></a></p>

Volt主题
<p align="center"><a href="http://www.primefaces.org/images/market/volt/components.png" title="点击查看大图"><img src="http://www.primefaces.org/images/market/volt/volt-theme.png" alt="Volt主题"/></a></p>

## 源码地址
primefaces-volt的GIT源代码位于

    http://git.oschina.net/ratking/primefaces-volt

primefaces-volt-demo的GIT源代码位于

    http://git.oschina.net/ratking/primefaces-volt-demo

## 官网原址
关于Volt的官方介绍，详见

    http://www.primefaces.org/layouts/volt.html

关于Volt的官方演示程序，详见

    http://www.primefaces.org/volt/

## 编译步骤
使用GIT客户端软件（比如git命令行或TortoiseGIT图形界面）下载上面的两个项目源代码后，
还需要maven编译打包，使用命令行：

    git clone http://git.oschina.net/ratking/primefaces-volt
    cd primefaces-volt
    mvn clean install

    git clone http://git.oschina.net/ratking/primefaces-volt-demo
    cd primefaces-volt-demo
    mvn clean package

也可以不用maven而使用NetBeans或Eclipse来编译打包。
