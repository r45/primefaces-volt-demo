/*
 * Copyright 2009-2015 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.primefaces.showcase.view.chart;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

@Named
@ViewScoped
public class ChartView implements Serializable {

    private LineChartModel lineModel;
    private BarChartModel barModel;

    @PostConstruct
    public void init() {
        createLineModels();
        createBarModels();
    }

    public LineChartModel getLineModel() {
        return lineModel;
    }

    public BarChartModel getBarModel() {
        return barModel;
    }

    private void createLineModels() {
        lineModel = initLinearModel();
        lineModel.setTitle("Linear Chart");
        lineModel.setLegendPosition("e");
        lineModel.setExtender("skinChart"); // 调用页面JavaScript函数
        lineModel.setAnimate(true);
        Axis yAxis = lineModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(10);
        yAxis.setTickCount(6);
    }

    private LineChartModel initLinearModel() {
        LineChartModel model = new LineChartModel();

        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("AUDI A3");

        series1.set(1, 2);
        series1.set(2, 1);
        series1.set(3, 3);
        series1.set(4, 6);
        series1.set(5, 8);

        LineChartSeries series2 = new LineChartSeries();
        series2.setLabel("BMW i8");

        series2.set(1, 6);
        series2.set(2, 3);
        series2.set(3, 2);
        series2.set(4, 7);
        series2.set(5, 9);

        LineChartSeries series3 = new LineChartSeries();
        series3.setLabel("Mercedes A200");

        series3.set(1, 4);
        series3.set(2, 6);
        series3.set(3, 9);
        series3.set(4, 3);
        series3.set(5, 2);

        model.addSeries(series1);
        model.addSeries(series2);
        model.addSeries(series3);

        return model;
    }

    private void createBarModels() {
        createBarModel();
    }

    private void createBarModel() {
        barModel = initBarModel();

        barModel.setTitle("Bar Chart");
        barModel.setExtender("skinChart"); // 调用页面JavaScript函数
        barModel.setAnimate(true);

        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setMin(0);
        xAxis.setMax(8.5);

        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(10);
    }

    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();

        ChartSeries series1 = new ChartSeries();
        series1.set(0, 8);
        series1.set(2, 1);
        series1.set(4, 9);
        series1.set(4.5, 2);
        series1.set(9, 7);

        ChartSeries series2 = new ChartSeries();
        series2.set(0, 1);
        series2.set(2, 9);
        series2.set(4, 2);
        series2.set(4.5, 8);
        series2.set(9, 1);

        ChartSeries series3 = new ChartSeries();
        series3.set(0, 9);
        series3.set(2, 7);
        series3.set(4, 1);
        series3.set(4.5, 5);
        series3.set(9, 3);

        model.addSeries(series1);
        model.addSeries(series2);
        model.addSeries(series3);

        return model;
    }
}
